/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import Header from "./header"
import Footer from "./footer"
import { Container } from "react-bootstrap"
import "bootstrap/dist/css/bootstrap.min.css"
import "../styles/global.scss"
import { injectIntl } from "gatsby-plugin-react-intl"

const Layout = ({ children, intl }) => {
  return (
    <>
      <Header
        siteTitle={intl.formatMessage({ id: "home.title" })}
        description={intl.formatMessage({ id: "home.description" })}
        moreInfo={intl.formatMessage({ id: "home.more_info" })}
      />
      <Container>
        <main>{children}</main>
      </Container>
      <Footer />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default injectIntl(Layout)
